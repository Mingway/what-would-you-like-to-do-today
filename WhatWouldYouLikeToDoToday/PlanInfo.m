//
//  PlanInfo.m
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-2.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PlanInfo.h"


@implementation PlanInfo

@dynamic completion_evaluation;
@dynamic evaluation_content;
@dynamic isNotify;
@dynamic plan_content;
@dynamic plan_date;
@dynamic plan_mark;

@end
