//
//  RootViewController.m
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-1.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "RootViewController.h"
#import "Utilities.h"
#import "PlanManager.h"
#import "PlanInfo.h"
#import "PlanViewController.h"

@interface RootViewController ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) NSMutableArray *todayPlanInfos;

@end

@implementation RootViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.todayPlanInfos = [NSMutableArray arrayWithArray:[[PlanManager defaultPlanInfo]getPlanInfosWithPlanMark:[Utilities getMarkByDate:[Utilities getToday]]]];
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *dateString = [Utilities getFormatDateString:[Utilities getToday]];
    NSString *weekString = [Utilities getWeekDayByDate:[Utilities getToday]];
    self.dateLabel.text = [NSString stringWithFormat:@"今天是%@    %@",dateString, weekString];
    self.dateLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.todayPlanInfos ? self.todayPlanInfos.count : 0;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"planCell" forIndexPath:indexPath];
        
        PlanInfo *planInfo = self.todayPlanInfos[indexPath.row];
        
        UILabel *dateLabel = (UILabel *)[cell viewWithTag:1];
        dateLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
        dateLabel.text = [Utilities getFormatTimeString:planInfo.plan_date];
        
        UILabel *contentLabel = (UILabel *)[cell viewWithTag:2];
        contentLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
        contentLabel.text = planInfo.plan_content;
        
        
        
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addCell" forIndexPath:indexPath];
        
        UILabel *addLabel = (UILabel *)[cell viewWithTag:1];
        addLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        PlanViewController *viewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PlanViewController"];
        viewController.mode = PlanViewControllerModeAdd;
        [self presentViewController:viewController animated:YES completion:nil];
    }else{
        PlanViewController *viewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PlanViewController"];
        viewController.mode = PlanViewControllerModeEdit;
        viewController.planInfo = self.todayPlanInfos[indexPath.row];
        [self presentViewController:viewController animated:YES completion:nil];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return YES;
    }else{
        return NO;
    }
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView endEditing:YES];
        [self deleteNotify:self.todayPlanInfos[indexPath.row]];
        
        NSManagedObject *eventToDelete = self.todayPlanInfos[indexPath.row];
		[[PlanManager defaultPlanInfo].managedObjectContext deleteObject:eventToDelete];
		
        [[PlanManager defaultPlanInfo].planInfos removeObject:self.todayPlanInfos[indexPath.row]];
        [[PlanManager defaultPlanInfo] saveContext];
        [self.todayPlanInfos removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (void)deleteNotify:(PlanInfo *)planInfo{
    NSArray *narry=[[UIApplication sharedApplication] scheduledLocalNotifications];
    NSUInteger acount=[narry count];
    if (acount>0){
        for (int i=0; i<acount; i++){
            UILocalNotification *myUILocalNotification = [narry objectAtIndex:i];
            NSDictionary *userInfo = myUILocalNotification.userInfo;
            if ([[userInfo objectForKey:@"nfkey"] isEqualToString:planInfo.plan_date.description]){
                [[UIApplication sharedApplication] cancelLocalNotification:myUILocalNotification];
                break;
            }
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}

@end
