//
//  AnalysisViewController.m
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-4.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "AnalysisViewController.h"
#import "PlanManager.h"
#import "PlanInfo.h"
#import "Utilities.h"

#import "PNChart.h"

@interface AnalysisViewController ()
@property (nonatomic, strong) NSArray *arr1;
@property (nonatomic, strong) NSArray *arr2;
@property (nonatomic, strong) NSArray *arr3;
@property (nonatomic, strong) NSArray *arr4;
@property (nonatomic, strong) NSArray *arr5;
@property (nonatomic, strong) NSArray *arr6;
@property (nonatomic, strong) NSArray *arr7;
@property (nonatomic, strong) NSMutableArray *datas;
@property (nonatomic, strong) NSMutableArray *labels;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation AnalysisViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.datas = [NSMutableArray array];
    self.labels = [NSMutableArray array];
    
    self.arr1 = [[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[self getPlanMarkByOffset:-1]];
    [self.datas addObject:@([self calculateAverageWithArr:self.arr1])];
    [self.labels addObject:[self getPlanMarkByOffset:-1]];
    
    self.arr2 = [[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[self getPlanMarkByOffset:-2]];
    [self.datas addObject:@([self calculateAverageWithArr:self.arr2])];
    [self.labels addObject:[self getPlanMarkByOffset:-2]];
    
    self.arr3 = [[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[self getPlanMarkByOffset:-3]];
    [self.datas addObject:@([self calculateAverageWithArr:self.arr3])];
    [self.labels addObject:[self getPlanMarkByOffset:-3]];
    
    self.arr4 = [[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[self getPlanMarkByOffset:-4]];
    [self.datas addObject:@([self calculateAverageWithArr:self.arr4])];
    [self.labels addObject:[self getPlanMarkByOffset:-4]];
    
    self.arr5 = [[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[self getPlanMarkByOffset:-5]];
    [self.datas addObject:@([self calculateAverageWithArr:self.arr5])];
    [self.labels addObject:[self getPlanMarkByOffset:-5]];
    
    /*
    self.arr6 = [[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[self getPlanMarkByOffset:-6]];
    [self.datas addObject:@([self calculateAverageWithArr:self.arr6])];
    [self.labels addObject:[self getPlanMarkByOffset:-6]];
    
    self.arr7 = [[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[self getPlanMarkByOffset:-7]];
    [self.datas addObject:@([self calculateAverageWithArr:self.arr7])];
    [self.labels addObject:[self getPlanMarkByOffset:-7]];
     */
    
    PNChart * barChart = [[PNChart alloc] initWithFrame:CGRectMake(0, 50.0, SCREEN_WIDTH, 200.0)];
	barChart.backgroundColor = [UIColor clearColor];
	barChart.type = PNBarType;
	[barChart setXLabels:self.labels];
	[barChart setYValues:self.datas];
	[barChart strokeChart];
	[self.scrollView addSubview:barChart];
}

- (NSString *)getPlanMarkByOffset:(int)dayOffset{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [NSDateComponents new];
    [offsetComponents setDay:dayOffset];
    NSDate *newDate = [gregorian dateByAddingComponents:offsetComponents toDate:[Utilities getToday] options:0];
    return [Utilities getMarkByDate:newDate];
}

- (float)calculateAverageWithArr:(NSArray*)arr{
    if (arr.count == 0 || !arr) {
        return 0.0;
    }
    float sum = 0;
    int i = 0;
    for (PlanInfo *planInfo in arr) {
        if (planInfo.completion_evaluation) {
            sum += [planInfo.completion_evaluation floatValue];
            i ++;
        }
    }
    return sum / i;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
