//
//  PlanViewController.h
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-1.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlanInfo.h"

typedef NS_ENUM(NSInteger, PlanViewControllerMode) {
    PlanViewControllerModeEdit = 0,
    PlanViewControllerModeAdd
};

@interface PlanViewController : UIViewController
@property (nonatomic, assign) PlanViewControllerMode mode;
@property (nonatomic, strong) PlanInfo *planInfo;

@end
