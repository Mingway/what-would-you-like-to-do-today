//
//  PlanViewController.m
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-1.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PlanViewController.h"
#import "MGConferenceDatePicker.h"
#import "MGConferenceDatePickerDelegate.h"
#import "Utilities.h"
#import "YFInputBar.h"
#import "PlanManager.h"

@interface PlanViewController ()<MGConferenceDatePickerDelegate, YFInputBarDelegate>{
    UIViewController *_datePickerViewController;
    YFInputBar *_inputBar;
}
@property (weak, nonatomic) IBOutlet UILabel *selectDateTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectDateButton;
@property (weak, nonatomic) IBOutlet UIButton *addPlanContentButton;
@property (weak, nonatomic) IBOutlet UILabel *addPlanContentTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *notifyTitleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *notifySwitch;
- (IBAction)addPlanContent:(UIButton *)sender;
- (IBAction)setPlanDate:(UIButton *)sender;
- (IBAction)close:(UIBarButtonItem *)sender;
- (IBAction)save:(UIBarButtonItem *)sender;
- (IBAction)switchEnableNotify:(UISwitch *)sender;

@end

@implementation PlanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectDateTitleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.addPlanContentTitleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.selectDateButton.titleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.addPlanContentButton.titleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.notifyTitleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    
    if (!self.planInfo) {
        self.planInfo = (PlanInfo*)[NSEntityDescription insertNewObjectForEntityForName:@"PlanInfo" inManagedObjectContext:[PlanManager defaultPlanInfo].managedObjectContext];
    }else{
        [self.selectDateButton setTitle:[Utilities getFormatTimeString:self.planInfo.plan_date] forState:UIControlStateNormal];
        [self.notifySwitch setOn:[self.planInfo.isNotify boolValue]];
        [self.addPlanContentButton setTitle:self.planInfo.plan_content forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)addPlanContent:(UIButton *)sender {
    if (!_inputBar) {
        _inputBar = [[YFInputBar alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY([UIScreen mainScreen].bounds)-44, 320, 44)];
        
        _inputBar.delegate = self;
        _inputBar.clearInputWhenSend = YES;
        _inputBar.resignFirstResponderWhenSend = YES;
    }
    if (![sender.titleLabel.text isEqualToString:@"点击添加内容"]) {
        _inputBar.textField.text = sender.titleLabel.text;
    }
    [_inputBar.textField becomeFirstResponder];
    [self.view addSubview:_inputBar];
}

- (IBAction)setPlanDate:(UIButton *)sender {
    if (!_datePickerViewController) {
        _datePickerViewController = [[UIViewController alloc]init];
        MGConferenceDatePicker *datePicker = [[MGConferenceDatePicker alloc] initWithFrame:self.view.bounds];
        [datePicker setDelegate:self];
        
        [datePicker setBackgroundColor:[UIColor whiteColor]];
        [_datePickerViewController setView:datePicker];
    }
    [self presentViewController:_datePickerViewController animated:YES completion:nil];
}

- (IBAction)close:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)save:(UIBarButtonItem *)sender {
    if (self.planInfo.plan_date && self.planInfo.plan_content) {
        self.planInfo.plan_mark = [Utilities getMarkByDate:self.planInfo.plan_date];
        self.planInfo.isNotify = @(self.notifySwitch.isOn);
        if (self.mode == PlanViewControllerModeAdd) {
            [[PlanManager defaultPlanInfo].planInfos insertObject:self.planInfo atIndex:0];
            [[PlanManager defaultPlanInfo] saveContext];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"请填写计划" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (IBAction)switchEnableNotify:(UISwitch *)sender {
    _planInfo.isNotify = @(sender.isOn);
    if (sender.isOn) {
        [self createNotify];
    }else{
        [self deleteNotify];
    }
}

// 添加本地通知
- (void)createNotify{
    if ([self.planInfo.plan_date timeIntervalSinceNow] > 0) {
        UILocalNotification *notification=[[UILocalNotification alloc] init];
        if (notification!=nil){
            notification.fireDate = self.planInfo.plan_date;
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.alertBody = self.planInfo.plan_content;
            notification.soundName = @"卡农铃声.mp3";
            notification.alertAction = self.planInfo.plan_content;
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.planInfo.plan_date.description,@"nfkey",nil];
            [notification setUserInfo:dict];
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
    }
}

// 手动删除通知
- (void)deleteNotify{
    NSArray *narry=[[UIApplication sharedApplication] scheduledLocalNotifications];
    NSUInteger acount=[narry count];
    if (acount>0){
        for (int i=0; i<acount; i++){
            UILocalNotification *myUILocalNotification = [narry objectAtIndex:i];
            NSDictionary *userInfo = myUILocalNotification.userInfo;
            if ([[userInfo objectForKey:@"nfkey"] isEqualToString:self.planInfo.plan_date.description]){
                [[UIApplication sharedApplication] cancelLocalNotification:myUILocalNotification];
                break;
            }
        }
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [((UIView*)obj) resignFirstResponder];
    }];
}

#pragma mark -
#pragma mark MGConferenceDatePickerDelegate

-(void)conferenceDatePicker:(MGConferenceDatePicker *)datePicker saveDate:(NSDate *)date {
    if (self.notifySwitch.isOn) {
        [self deleteNotify];
    }
    self.planInfo.plan_date = date;
    [self.selectDateButton setTitle:[Utilities getFormatTimeString:date] forState:UIControlStateNormal];
    [_datePickerViewController dismissViewControllerAnimated:YES completion:nil];
    if (self.notifySwitch.isOn) {
        [self createNotify];
    }
}

- (void)cancelConferenceDatePicker:(MGConferenceDatePicker *)datePicker{
    [_datePickerViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark YFInputBarDelegate

-(void)inputBar:(YFInputBar *)inputBar withInputString:(NSString *)str
{
    if (self.notifySwitch.isOn) {
        [self deleteNotify];
    }
    self.planInfo.plan_content = str;
    [self.addPlanContentButton setTitle:str forState:UIControlStateNormal];
    [_inputBar removeFromSuperview];
    if (self.notifySwitch.isOn) {
        [self createNotify];
    }
}

@end
