//
//  EvaluateViewController.h
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-2.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlanInfo.h"

@interface EvaluateViewController : UIViewController
@property (nonatomic, strong) PlanInfo *planInfo;

@end
