//
//  Utilities.h
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-1.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject
+ (NSDate *)getToday;
+ (NSString *)getFormatDateString:(NSDate *)date;
+ (NSString *)getWeekDayByDate:(NSDate *)date;
+ (NSString *)getFormatTimeString:(NSDate *)date;
+ (NSString *)getMarkByDate:(NSDate *)date;

@end
