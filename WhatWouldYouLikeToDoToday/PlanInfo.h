//
//  PlanInfo.h
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-2.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PlanInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * completion_evaluation;
@property (nonatomic, retain) NSString * evaluation_content;
@property (nonatomic, retain) NSNumber * isNotify;
@property (nonatomic, retain) NSString * plan_content;
@property (nonatomic, retain) NSDate * plan_date;
@property (nonatomic, retain) NSString * plan_mark;

@end
