//
//  MoreViewController.m
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-1.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MoreViewController.h"

@interface MoreViewController ()
@property (weak, nonatomic) IBOutlet UILabel *settingLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedbackLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkUpdateLabel;
@property (weak, nonatomic) IBOutlet UILabel *evaluateLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;

@end

@implementation MoreViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.settingLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.feedbackLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.checkUpdateLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.evaluateLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.aboutLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
