//
//  BeforeViewController.m
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-1.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "BeforeViewController.h"
#import "Utilities.h"
#import "PlanManager.h"
#import "PlanInfo.h"
#import "EvaluateViewController.h"

@interface BeforeViewController ()
@property (nonatomic, strong) NSMutableArray *planInfos;
@property (nonatomic, strong) NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet UIButton *preDayButton;
@property (weak, nonatomic) IBOutlet UIButton *nextDayButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
- (IBAction)switchPreDay:(UIButton *)sender;
- (IBAction)switchNextDay:(UIButton *)sender;

@end

@implementation BeforeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self checkCurrentDay];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.planInfos = [NSMutableArray array];
    self.dateLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.selectedDate = [Utilities getToday];
    [self switchToDay:-1];
}

- (void)checkCurrentDay{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *componentsForFirstDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:self.selectedDate];
    NSDateComponents *componentsForSecondDate = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[self getYesterday]];
    
    if ([componentsForFirstDate year] == [componentsForSecondDate year]){
        if ([componentsForFirstDate month] == [componentsForSecondDate month]) {
            if ([componentsForFirstDate day] == [componentsForSecondDate day]) {
                self.nextDayButton.hidden = YES;
            }else if([componentsForFirstDate day] < [componentsForSecondDate day]){
                self.nextDayButton.hidden = NO;
            }else{
                self.nextDayButton.hidden = YES;
            }
        }else if ([componentsForFirstDate month] < [componentsForSecondDate month]){
            self.nextDayButton.hidden = NO;
        }else{
            self.nextDayButton.hidden = YES;
        }
    }else if([componentsForFirstDate year] < [componentsForSecondDate year]){
        self.nextDayButton.hidden = NO;
    }else{
        self.nextDayButton.hidden = YES;
    }
    
    self.planInfos = [NSMutableArray arrayWithArray:[[PlanManager defaultPlanInfo] getPlanInfosWithPlanMark:[Utilities getMarkByDate:self.selectedDate]]];
    [self.tableView reloadData];
}

- (NSDate *)getYesterday{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];
    
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    NSDate *today = [cal dateByAddingComponents:components toDate:[[NSDate alloc] init] options:0];
    
    [components setHour:-24];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
    
    return yesterday;
}

- (void)switchToDay:(NSInteger)dayOffset {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [NSDateComponents new];
    [offsetComponents setDay:dayOffset];
    NSDate *newDate = [gregorian dateByAddingComponents:offsetComponents toDate:self.selectedDate options:0];
    self.selectedDate = newDate;
    
    [self.dateLabel setText:[self stringFromDate:self.selectedDate withFormat:@"dd LLLL yyyy"]];
}

- (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)format {
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans"];
    [formatter setLocale:locale];
    [formatter setDateFormat:format];
    
    return [formatter stringFromDate:date];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.planInfos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"planCell" forIndexPath:indexPath];
    
    PlanInfo *planInfo = self.planInfos[indexPath.row];
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:1];
    dateLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    dateLabel.text = [Utilities getFormatTimeString:planInfo.plan_date];
    
    UILabel *contentLabel = (UILabel *)[cell viewWithTag:2];
    contentLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    contentLabel.text = planInfo.plan_content;
    
    UILabel *evaluationContentLabel = (UILabel *)[cell viewWithTag:3];
    evaluationContentLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:12.f];
    if (planInfo.evaluation_content) {
        evaluationContentLabel.text = planInfo.evaluation_content;
    }else{
        evaluationContentLabel.text = @"未添加评价内容";
    }
    
    UILabel *evaluationLabel = (UILabel *)[cell viewWithTag:4];
    evaluationLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    if (planInfo.completion_evaluation) {
        evaluationLabel.text = [@([planInfo.completion_evaluation floatValue] * 10) stringValue];
    }else{
        evaluationLabel.text = @"待评";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    EvaluateViewController *evaluateViewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"EvaluateViewController"];
    evaluateViewController.planInfo = self.planInfos[indexPath.row];
    [self presentViewController:evaluateViewController animated:YES completion:nil];
}

- (IBAction)switchPreDay:(UIButton *)sender {
    [self switchToDay:-1];
    [self checkCurrentDay];
}

- (IBAction)switchNextDay:(UIButton *)sender {
    [self switchToDay:1];
    [self checkCurrentDay];
}
@end
