//
//  EvaluateViewController.m
//  WhatWouldYouLikeToDoToday
//
//  Created by developer on 14-5-2.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "EvaluateViewController.h"
#import "Utilities.h"
#import "TQStarRatingView.h"
#import "YFInputBar.h"
#import "PlanManager.h"

@interface EvaluateViewController () <StarRatingViewDelegate, YFInputBarDelegate>{
    YFInputBar *_inputBar;
}
@property (weak, nonatomic) IBOutlet UILabel *planDateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *planDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *planContentTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *planContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *evaluateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *evaluationContentTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *evaluateScoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *evaluationContentButton;
@property (strong, nonatomic) TQStarRatingView *starRatingView;
- (IBAction)addEvaluationContent:(UIButton *)sender;
- (IBAction)close:(UIBarButtonItem *)sender;

@end

@implementation EvaluateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[PlanManager defaultPlanInfo] saveContext];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.planDateTitleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.planDateLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.planContentTitleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.planContentLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.evaluateTitleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.evaluationContentTitleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    self.evaluationContentButton.titleLabel.font = [UIFont fontWithName:@"DFWaWaSC-W5" size:17.f];
    
    self.planDateLabel.text = [Utilities getFormatTimeString:self.planInfo.plan_date];
    self.planContentLabel.text = self.planInfo.plan_content;
    if (self.planInfo.evaluation_content) {
        [self.evaluationContentButton setTitle:self.planInfo.evaluation_content forState:UIControlStateNormal];
    }
    
    self.starRatingView = [[TQStarRatingView alloc] initWithFrame:CGRectMake(20, 190, 150, 30) numberOfStar:5];
    self.starRatingView.delegate = self;
    if (self.planInfo.completion_evaluation) {
        [self.starRatingView setScore:[self.planInfo.completion_evaluation floatValue]];
        self.evaluateScoreLabel.text = [@([self.planInfo.completion_evaluation floatValue] * 10) stringValue];
    }else{
        self.evaluateScoreLabel.text = @"未评分";
    }
    [self.view addSubview:self.starRatingView];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [((UIView*)obj) resignFirstResponder];
    }];
}

#pragma mark -
#pragma mark StarRatingViewDelegate

-(void)starRatingView:(TQStarRatingView *)view score:(float)score
{
    self.planInfo.completion_evaluation = @(score);
    self.evaluateScoreLabel.text = [@(score * 10) stringValue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addEvaluationContent:(UIButton *)sender {
    if (!_inputBar) {
        _inputBar = [[YFInputBar alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY([UIScreen mainScreen].bounds)-44, 320, 44)];
        
        _inputBar.delegate = self;
        _inputBar.clearInputWhenSend = YES;
        _inputBar.resignFirstResponderWhenSend = YES;
    }
    if (![sender.titleLabel.text isEqualToString:@"点击添加评价"]) {
        _inputBar.textField.text = sender.titleLabel.text;
    }
    [_inputBar.textField becomeFirstResponder];
    [self.view addSubview:_inputBar];
}

- (IBAction)close:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark YFInputBarDelegate

-(void)inputBar:(YFInputBar *)inputBar withInputString:(NSString *)str
{
    self.planInfo.evaluation_content = str;
    [self.evaluationContentButton setTitle:str forState:UIControlStateNormal];
    [_inputBar removeFromSuperview];
}

@end
